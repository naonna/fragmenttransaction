package com.example.okorchysta.fragmenttransaction.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SwipeableViewPager extends ViewPager {

    private boolean disableSwipe = false;

    public SwipeableViewPager(@NonNull Context context) {
        super(context);
    }

    public SwipeableViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return !disableSwipe && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return !disableSwipe && super.onTouchEvent(event);
    }

    public void disableScroll(Boolean disable){
        this.disableSwipe = disable;
    }
}
