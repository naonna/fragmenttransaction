package com.example.okorchysta.fragmenttransaction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnNav = findViewById(R.id.btn_navigation);
        Button btnTab = findViewById(R.id.btn_tab);
        btnNav.setOnClickListener(this);
        btnTab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.btn_navigation :
                intent = new Intent(this, NavigationActivity.class);
                break;
            case R.id.btn_tab :
                intent = new Intent(this, TabActivity.class);
                break;
        }
        if (intent!=null)
            startActivity(intent);
    }
}
