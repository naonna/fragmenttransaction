package com.example.okorchysta.fragmenttransaction.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.okorchysta.fragmenttransaction.R;

public class FragmentContainer extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_container, container, false);
        Fragment fragmentTop = new FirstFragment();
        Fragment fragmentBottom = new BottomFragment();

        getChildFragmentManager().beginTransaction()
                .replace(R.id.fragment_top, fragmentTop).commit();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.fragment_bottom, fragmentBottom).commit();
        return view;
    }
}