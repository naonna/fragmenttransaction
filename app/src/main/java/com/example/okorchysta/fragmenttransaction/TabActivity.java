package com.example.okorchysta.fragmenttransaction;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.okorchysta.fragmenttransaction.fragments.FirstFragment;
import com.example.okorchysta.fragmenttransaction.fragments.FragmentContainer;
import com.example.okorchysta.fragmenttransaction.fragments.SecondFragment;
import com.example.okorchysta.fragmenttransaction.utils.SwipeableViewPager;
import com.example.okorchysta.fragmenttransaction.utils.ViewPagerAdapter;

public class TabActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        final TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Fist"));
        tabLayout.addTab(tabLayout.newTab().setText("Second"));
        tabLayout.addTab(tabLayout.newTab().setText("TwoFragm"));

        SwipeableViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.disableScroll(
                true);
        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Fragment fragment1 = new FirstFragment();
        Fragment fragment2 = new SecondFragment();
        Fragment fragment3 = new FragmentContainer();

        adapter.addFragment(fragment1, "First");
        adapter.addFragment(fragment2, "Second");
        adapter.addFragment(fragment3, "TwoFragm");

        viewPager.setAdapter(adapter);



    }
}
