package com.example.okorchysta.fragmenttransaction;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.okorchysta.fragmenttransaction.fragments.FirstFragment;
import com.example.okorchysta.fragmenttransaction.fragments.FragmentContainer;
import com.example.okorchysta.fragmenttransaction.fragments.SecondFragment;

public class NavigationActivity extends AppCompatActivity {

    BottomNavigationView bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        final Fragment fragment1 = new FirstFragment();
        final Fragment fragment2 = new SecondFragment();
        final Fragment fragment3 = new FragmentContainer();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment1).commit();

        bottomNav = findViewById(R.id.bottom_navigation);

        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                bottomNav.getSelectedItemId();
                switch (menuItem.getItemId()){
                    case R.id.action_first_fragment :
                        launchFragment(fragment1, true);
                        break;
                    case R.id.action_second_fragment :
                        launchFragment(fragment2, true);
                        break;
                    case R.id.action_two_fragments :
                        launchFragment(fragment3, true);
                        break;
                }

                return true;
            }
        });

    }

    public void launchFragment(Fragment fragment, boolean addToBackStack){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.container, fragment);
        if (addToBackStack)
                transaction.addToBackStack(fragment.getTag());
        transaction.commit();
    }
}
